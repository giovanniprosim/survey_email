import request from 'supertest'
import { getConnection } from 'typeorm'
import { app } from '../app'

import createConnection from "../database"

describe("Survey", () => {
  beforeAll(async () => {
    const connection = await createConnection()
    await connection.runMigrations()
  })

  afterAll(async () => {
    const connection = getConnection()
    await connection.dropDatabase()
    await connection.close()
  })

  it("Should be able to create a new survey", async () => {
    const resp = await request(app).post("/surveys")
    .send({
      title: "Title",
      description: "Description Example",
    })

    expect(resp.status).toBe(201)
    expect(resp.body.survey).toHaveProperty("id")
  })

  it("Should be able to get a list of surveys", async () => {
    const resp = await request(app).get("/surveys")

    expect(resp.status).toBe(200)
    expect(resp.body[1]).toBeGreaterThanOrEqual(1)
  })

})
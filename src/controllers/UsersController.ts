import { Request, Response } from 'express'
import { getCustomRepository } from 'typeorm'
import { UsersRepository } from '../repositories/UsersRepository'
import * as yup from 'yup'

import { AppError } from '../errors/AppError'

class UsersController {
  async create(req: Request, resp: Response) {
    const { name, email } = req.body

    const schema = yup.object().shape({
      name: yup.string().required("O nome é obrigatório."),
      email: yup.string().email().required("O e-mail deve ser válido.")
    })

    try {
      await schema.validate(req.body, { abortEarly: false })
    } catch (err) {
      throw new AppError(err)
    }

    const usersRepository = getCustomRepository(UsersRepository)

    const userAlreadyExists = await usersRepository.findOne({
      email
    })
    
    if(userAlreadyExists) throw new AppError(`The email '${email}' already exists...`)

    const user = usersRepository.create({
      name, email
    })

    await usersRepository.save(user)

    return resp.status(201).json({message: "User created with success!", user})
  }

  async show(req: Request, resp: Response) {
    const usersRepository = getCustomRepository(UsersRepository)

    const all = await usersRepository.findAndCount()

    return resp.status(200).json(all)
  }
}

export { UsersController }

1- Environment
  * TypeORM
  * migrations
  * controllers
  * models
  * views
  * repositories
  * sqlite3

2- Tests
  * jest
  * supertests

3- Send Mails
  * nodemailer
  * ethereal (https://ethereal.email/)
  * handlebars (email templates)

4- Validation
  * YUP

5- Error Treament
  * App Error (throw new AppError)
  * Error middleware
